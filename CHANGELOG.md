# GitLab Omnibus Sandbox All-in-One Terraform Module

## 0.2.0

* [#8](https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gitlab/gitlab-omnibus-sandbox-tf-module/-/issues/8) Add GitLab Runner Manager for Docker Machine autoscaling

## 0.1.0

* Initial release with tested resources
